/*
 * Copyright (C) 2020 Szabo, Zsolt Gyorgy <szabo.zsolt.gyorgy@gmail.com>
 *
 * This file is part of Rise of Civilizations - Server.
 *
 * Rise of Civilizations - Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Rise of Civilizations - Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Rise of Civilizations - Server.  If not, see <https://www.gnu.org/licenses/>.
 */
package us.trantor.riseofcivilizations.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"app.name=test-integration-app-name", "app.version=v2.2.2"})
class AboutControllerIntegrationTest {

    private static final String EXPECTED_APP_VERSION = "v2.2.2";
    private static final String EXPECTED_APP_NAME = "test-integration-app-name";
    private final MockMvc mvc;

    @Autowired
    AboutControllerIntegrationTest(final MockMvc mvc) {
        this.mvc = mvc;
    }

    @Test
    void givenApplicationInformation_whenAppInfoEndpointCalled_thenStatus200() throws Exception {
        mvc.perform(get("/about/appInfo").contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.appName", is(EXPECTED_APP_NAME)))
                .andExpect(jsonPath("$.appVersion", is(EXPECTED_APP_VERSION)));
    }
}
