FROM gradle:jdk11 as BUILD
COPY . /usr/src/app
RUN gradle -b /usr/src/app/build.gradle clean assemble

FROM openjdk:11-slim
ENV PORT 8080
EXPOSE 8080
COPY --from=BUILD /usr/src/app/build/libs /opt/build
WORKDIR /opt/build

CMD ["/bin/bash", "-c", "find -type f -name '*.jar' | xargs java -jar"]
